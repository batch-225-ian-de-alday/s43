let posts = []; // store posts in memory
let count = 1; // keep track of post id


// add post data to posts array
document.querySelector("#form-add-post").addEventListener("submit", (event) => {
    
    event.preventDefault(); // prevent default form submission

    // add data to posts array
    posts.push({
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    });
    // this will create the ID for each post id1, id2, id3, etc
    count++;

    console.log(posts);
    showPosts(posts); // display posts
    alert("Post added successfully");
   
});


// Show posts
const showPosts = (posts) => {

    // create post entries for the values in the posts array
    let postEntries = "";

    // loop through posts array
    posts.forEach((post) => {

        // create post entries
        postEntries += `
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost(${post.id})">Edit</button>
            <button onclick="deletePost(${post.id})">Delete</button>
        </div>
        `;
    });

    // display post entries
    document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit post
// UI
const editPost = (id) => {  
    // get post title and body
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}



// Update post
// Backend

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

    e.preventDefault()

    for (let i = 0; i < posts.length; i++) {

        // check if postId and txt-edit-id value is correct
        // the value posts[i].id is a number, so we need to convert it to a string to compare it with the value of the document.querySelector('#txt-edit-id').value because it is a string
        // therefore, we use the toString() method to convert the number to a string
        isIdCorrect = posts[i].id.toString() === document.querySelector('#txt-edit-id').value;

        if (isIdCorrect) {

            // Logic
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;

        }        

        showPosts(posts);
        alert('Post successfully updated.');

    }

})

// ============ Activity ============
// delete post
/*
    The code document.querySelector(#post-${id}).remove(); is used to remove an HTML element from the web page. The document.querySelector method is used to select the first element that matches a CSS selector. In this case, the selector #post-${id} selects an element with an id attribute equal to "post-" followed by the value of the variable id. The remove method is then called on the selected element to remove it from the web page.
*/
const deletePost = (id) => {
    posts = posts.filter((post) => {
        if (post.id.toString()== id) {
            return post;
        }
    })

    document.querySelector(`#post-${id}`).remove();

}

// delete post
// UI
// const deletePost = (id) => {  
//     // get post title and body
//     let title = document.querySelector(`#post-title-${id}`).innerHTML;
//     let body = document.querySelector(`#post-body-${id}`).innerHTML;

//     document.querySelector('#txt-edit-id').value = id;
//     document.querySelector('#txt-edit-title').value = title;
//     document.querySelector('#txt-edit-body').value = body;
// }


// Update delete post
// Backend

// document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

//     e.preventDefault()

//     for (let i = 0; i < posts.length; i++) {

//         // check if postId and txt-edit-id value is correct
//         // the value posts[i].id is a number, so we need to convert it to a string to compare it with the value of the document.querySelector('#txt-edit-id').value because it is a string
//         // therefore, we use the toString() method to convert the number to a string
//         isIdCorrect = posts[i].id.toString() === document.querySelector('#txt-edit-id').value;

//         if (isIdCorrect) {

//             // Logic on how to delete a post
//             posts.splice(i, 1);
//         }

//         showPosts(posts);
//         alert('Post successfully deleted.');

//     }

// })






